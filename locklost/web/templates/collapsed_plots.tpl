<br />
<div class="panel-group">
<div class="panel panel-default">

<div class="panel-heading">
<h5 class="panel-title"><a data-toggle="collapse" href="#{{id}}">{{title}} (click to show)</a></h5>
</div>

<div id="{{id}}" class="panel-collapse collapse">
<div class="panel-body">

<div class="row">
% for plot_url in plots:
    <div class="col-md-{{size}}">
    <div class="thumbnail">
    <a href="{{plot_url}}" target="_blank"><img src="{{plot_url}}" style="width:100%" class="img-responsive" alt="" /></a>
    </div>
    </div>
    <br />
% end
</div>

</div>
</div>

</div>
</div>
