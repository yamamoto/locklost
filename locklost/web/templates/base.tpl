<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title>{{IFO}} lock losses</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
% if refresh:
    <meta http-equiv="refresh" content="30">
% end
<link rel="stylesheet" href="https://bootswatch.com/4/litera/bootstrap.min.css" media="screen">
</head>
<body>
<div class="container">
<h2></h2>
<h2><a href="{{web_script}}">{{IFO}} lock losses</a></h2>
<h6>{{date}}</h6>
{{!online_status}}
<br />

<!-- locklost urls -->
<a href="https://git.ligo.org/jameson.rollins/locklost">git repo </a>
<a href="https://git.ligo.org/jameson.rollins/locklost/blob/master/README.md">docs </a>
<a href="https://git.ligo.org/jameson.rollins/locklost/issues">issue tracker </a>

<!-- event form -->
<hr />
<div class="container">
<form method="get" action="{{web_script}}">
 <div class="row">
 show event:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="GPS time" name="event">
   </div>
 </div>
</form>
</div>

<!-- event filter form -->
<br />
<div class="container">
<form method="get" action="{{web_script}}">
 <div class="row">
 filter events:
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="after GPS" name="after">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="before GPS" name="before">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="guardian state" name="state">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="tag" name="tag">
   </div>
   <div class="col-sm-2">
     <input type="text" class="form-control" placeholder="# events to display" name="limit">
   </div>
   <div class="col-sm-2">
     <button type="submit" class="btn btn-primary mb-2">Submit</button>
   </div>
 </div>
</form>
</div>
<hr />

{{!base}}

</div>
<br />
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" 
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous">
</script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js"
        integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous">
</script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"
        integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous">
</script>
<script>
  jQuery(document).ready(function($) {
    $(".clickable-row").click(function() {
        window.location = $(this).data("href");
    });
  });
</script>
</body>
</html>
