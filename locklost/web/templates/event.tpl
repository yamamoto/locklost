% rebase('base.tpl', IFO=IFO, web_script=web_script, date=date, online_status=online_status)

% import os
% import glob
% from locklost import config
% from locklost.web.utils import analysis_status_button, event_plot_urls

<!-- event status button -->
% status_button = analysis_status_button(event)
{{!status_button}}

<!-- event overview -->
% prev, lockloss = event.transition_index
<h3>lockloss: <a href="{{event.url}}">{{event.id}}</a></h3>
<h5>GPS {{event.gps}}</h5>
<h5>{{event.utc}}</h5>
<h6>state transition: <tt>{{config.GRD_NODE}}</tt>  {{prev}} -> {{lockloss}}</h6>
<h6>tags: {{!tags}}</h6>

<!-- refined gps -->
<hr />
<div class="container">
<div class="row">
% include('plots.tpl', plots=event_plot_urls(event, 'indicators'), size=6)
</div>
</div>

<!-- saturation plots -->
% csv_path = event.gen_path('saturations.csv')
% saturations = os.path.exists(csv_path)
% if saturations:
      <hr />
      <h3>Saturation Plots</h3>
      <br />
      <div class="row">
%     include('plots.tpl', plots=event_plot_urls(event, 'saturations'), size=6)
      </div>

      <!-- saturation table -->
      <div class="panel-group">
      <div class="panel panel-default">
      <div class="panel-heading">
      <h5 class="panel-title"><a data-toggle="collapse" href="#sat1">Saturations table by channel (click to show)</a></h5>
      </div>
      <div id="sat1" class="panel-collapse collapse">
      <div class="panel-body">
      <table class="table table-condensed table-hover">
      <thead>

      <tr>
      <th>Channel</th>
      <th>Time to first saturation</th>
      </tr>
      </thead>
      <tbody>

      % for channel, time in sat_channels:
            <tr>
            <div class="row">
            <td>{{channel}}</td>
            <td>{{time}}</td>
            </div>
            </tr>
      %  end

      </tbody>
      </table>
      </div>
      </div>
      </div>
      </div>

% else:
      <p>No saturating suspension channels before refined lockloss time.</p>
% end

<!-- LPY plots -->
% if saturations:
      <hr />
      <div class="container">
      <h3>Length-Pitch-Yaw Plots</h3>
      <br />
      <p>DAC counts for first suspension stage to saturate</p>
      <div class="row">
%     include('plots.tpl', plots=event_plot_urls(event, 'lpy'), size=6)
      </div>
      </div>
% else:
      <p>LPY plots not created due to lack of saturating suspension channels.</p>
% end

<!-- BRS glitch -->
<hr />
<div class="container">
<br />
<div class="row">
% brs_plots = []
% for endstation in ['ETMX', 'ETMY']:
%       brs_url = event.gen_url('brs_%s.png' % (endstation))
%       brs_plots.append(brs_url)
%       end
% include('collapsed_plots.tpl', title ='BRS plots', id='brs', plots=brs_plots, size=5)
</div>
</div>

<!-- analog board saturation plot -->
<hr />
<div class="container">
<br />
<div class="row">
% board_plot = [event.gen_url('board_sat.png')]
% include('collapsed_plots.tpl', title ='Analog board saturation plots', id='board_sat', plots=board_plot, size=5)
</div>
</div>

<!-- environment plot -->
<hr />
<div class="container">
<br />
<div class="row">
% env_plots = [
% event.gen_url('windy.png'),
% event.gen_url('seismic.png'),
% ]
% include('collapsed_plots.tpl', title ='Environment plots', id='environment', plots=env_plots, size=5)
</div>
</div>

<!-- ADS excursion plot -->
<hr />
<div class="container">
<br />
<div class="row">
% ads_plot = [event.gen_url('ads.png')]
% include('collapsed_plots.tpl', title ='ADS Excursion Plot', id='ads_excursion', plots=ads_plot, size=5)
</div>
</div>

<!-- ADC overflows -->
<hr />
<div class="container">
<h3>ADC Overflow Plots</h3>
<br />
<h5>Overview</h5>
<div class="row">
% include('plots.tpl', title='ADC Overflows Overview', plots=event_plot_urls(event, 'adc_overflow_overview'), size=6)
</div>
%
% for ii, subsystem in enumerate(['ASC', 'LSC', 'OMC']):
%     adc_name = 'adc_overflow_{}'.format(subsystem)
%     adc_plots = sorted(glob.glob(event.gen_path('adc_overflow_{}_*.png'.format(subsystem))))
%     include('collapsed_plots.tpl', title='All {} overflows'.format(subsystem), id=adc_name, plots=adc_plots, size=6)
% end
</div>

<!-- glitch heatmaps -->
<hr />
<div class="container">
<h3>Glitch Heatmaps</h3>
<br />
% for ii, subsystem in enumerate(config.GLITCH_CHANNELS.keys()):
%     plot_name = 'snr_eventmap_{}'.format(subsystem)
%     plot_urls = event_plot_urls(event, plot_name)
      <div class="row">
%     if not plot_urls:
         <p>No glitch plots available for {{subsystem}} subsystem</p>
%     else:
%         include('plots.tpl', plots=plot_urls, size=6)
%     end
      </div>
% end
</div>

<!-- ASC/LSC timeseries plots -->
<hr />
<div class="container">
<h3>LSC/ASC Plots</h3><br />
<p> Lists of relevant LSC/ASC channels </p>
% for chan_group, chan_list in config.LSC_ASC_CHANNELS.items():
      <h5>{{chan_group}}</h5>
      <div class="row">
%     for channel in chan_list:
%         include('plots.tpl', plots=event_plot_urls(event, channel), size=3)
%     end
      </div>
      <br />
% end
</div>
