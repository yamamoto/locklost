from __future__ import print_function, division
import signal
import logging

import matplotlib
matplotlib.use('Agg')

try:
    from .version import version as __version__
except ImportError:
    __version__ = '?.?.?'

from . import config

# signal handler kill/stop signals
def signal_handler(signum, frame):
    try:
        signame = signal.Signal(signum).name
    except AttributeError:
        signame = signum
    logging.error("Signal received: {} ({})".format(signame, signum))
    raise SystemExit(1)

def set_signal_handlers():
    signal.signal(signal.SIGINT, signal_handler)
    signal.signal(signal.SIGTERM, signal_handler)
    signal.signal(eval('signal.'+config.CONDOR_KILL_SIGNAL), signal_handler)
