import os
import logging
import numpy as np

from gwpy.segments import Segment

from .. import config
from .. import data

##################################################

INITIAL_WINDOW = [-60, 0]

def find_previous_state(event):
    """Collect info about previous guardian state

    Writes out a 'previous_state' file with format:

    previous_state start_gps end_gps lockloss_state

    """
    channels = [config.GRD_STATE_N_CHANNEL]

    # get the lock loss transition itself
    previous_index, lockloss_index = event.transition_index

    # find the transition time
    segment = Segment(0, 1).shift(event.id)
    gbuf = data.fetch(channels, segment)[0]
    lli = np.where(gbuf.data == lockloss_index)[0]
    assert len(lli) > 0, "Lock loss not found at this time!"
    state_end_gps = gbuf.tarray[lli[0]]
    # note the transition time for old events
    state_end_file = event.gen_path('guard_state_end_gps')
    if not os.path.exists(state_end_file):
        with open(state_end_file, 'w') as f:
            f.write('{:f}\n'.format(state_end_gps))
    assert state_end_gps == event.transition_gps, "State end time does not match transition_gps ({} != {}).".format(state_end_gps, event.transition_gps)

    # find start of previous state
    lockloss_found = False
    state_start_gps = None
    power = 1
    window = [INITIAL_WINDOW[0], 1]

    while not state_start_gps:
        # define search window and search for change in guardian state
        segment = Segment(*window).shift(state_end_gps)
        gbuf = data.fetch(channels, segment)[0]
        transitions = list(data.gen_transitions(gbuf))

        # check the transitions
        for transition in reversed(transitions):
            if lockloss_found:
                assert int(transition[2]) == previous_index, "Transition history does not match lock loss transtion ({} != {}).".format(transition[2], previous_index)
                state_start_gps = transition[0]
                break
            elif tuple(transition[1:]) == event.transition_index:
                logging.info("lockloss found: {}".format(transition[0]))
                lockloss_found = True
        assert lockloss_found, "lock loss not found in first segment"

        window = [edge * (2 ** power) + window[0] for edge in INITIAL_WINDOW]
        power += 1

    # write start of lock stretch to disk
    previous_state = (previous_index, state_start_gps, state_end_gps, lockloss_index)
    logging.info("previous guardian state: {}".format(previous_state))
    with open(event.gen_path('previous_state'), 'w') as f:
        f.write('{} {:f} {:f} {}\n'.format(*previous_state))
