import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

#################################################

def check_ads(event):
    """Checks for ADS channels above threshold.

    Checks if ADS channels exceed threshold in the time before
    lockloss. Should not happen for fast locklosses.

    """

    if event.transition_index[0] != config.GRD_NOMINAL_STATE[1]:
        logging.info('lockloss not from nominal low noise')
        return

    plotutils.set_rcparams()

    mod_window = [config.ADS_SEARCH_WINDOW[0], config.ADS_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    ads_channels = data.fetch(config.ADS_CHANNELS, segment)

    saturating = False
    max_ads = 0
    thresh_crossing = segment[1]
    for buf in ads_channels:
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        before_lockloss = np.where(t-event.gps < 0)[0]
        max_ads = max([max_ads, max(abs(buf.data[before_lockloss]))])
        if any(abs(buf.data[before_lockloss]) > config.ADS_THRESH):
            saturating = True
            glitch_idx = np.where(abs(buf.data) > config.ADS_THRESH)[0][0]
            glitch_time = t[glitch_idx]
            thresh_crossing = min(glitch_time, thresh_crossing)

    if saturating:
        event.add_tag('ADS_EXCURSION')
    else:
        logging.info('no ADS excursion detected')
        
    fig, ax = plt.subplots(1, figsize=(22,16))
    for idx, buf in enumerate(ads_channels):
        srate = buf.sample_rate
        t = np.arange(segment[0], segment[1], 1/srate)
        ax.plot(
            t-event.gps,
            buf.data,
            label=buf.channel,
            alpha=0.8,
            lw=2,
            color=config.BOARD_SAT_CM[idx],
        )
    ax.axhline(
        config.ADS_THRESH,
        linestyle='--',
        color='black',
        label='ADS excursion threshold',
        lw=5,
    )
    ax.axhline(
        -config.ADS_THRESH,
        linestyle='--',
        color='black',
        lw=5,
    )
    if thresh_crossing != segment[1]:
        plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

    if max_ads < config.ADS_THRESH:
        ymin, ymax = -config.ADS_THRESH-0.01, config.ADS_THRESH+0.01
    else:
        ymin, ymax = -max_ads-0.01, max_ads+0.01
    ax.grid()
    ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
    ax.set_ylabel('ADS Counts')
    ax.set_ylim(ymin, ymax)
    ax.set_xlim(t[0]-event.gps, t[-1]-event.gps)
    ax.legend(loc='best')
    ax.set_title('ADS Channels Before Lockloss', y=1.04)
    fig.tight_layout(pad=0.05)

    outfile_plot = 'ads.png'
    outpath_plot = event.gen_path(outfile_plot)
    fig.savefig(outpath_plot, bbox_inches='tight')
