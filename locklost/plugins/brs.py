import logging
import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

#################################################

def check_brs(event):
    """Checks for BRS glitches at both end stations.

    Checks if more than one of three BRS channels is above threshold around
    time of lockloss (at both endstations) and creates tag/plot if it is.

    """

    plotutils.set_rcparams()

    mod_window = [config.BRS_SEARCH_WINDOW[0], config.BRS_SEARCH_WINDOW[1]]
    segment = Segment(mod_window).shift(int(event.gps))
    for endstation, channel_names in config.BRS_CHANNELS.iteritems():
        brs_channels = data.fetch(channel_names, segment)

        glitch_count = 0
        max_brs = 0
        thresh_crossing = segment[1]
        for buf in brs_channels:
            max_brs = max([max_brs, max(buf.data)])
            if any(buf.data > config.BRS_THRESH):
                glitch_count += 1
                srate = buf.sample_rate
                t = np.arange(segment[0], segment[1], 1/srate)
                glitch_idx = np.where(buf.data > config.BRS_THRESH)[0][0]
                glitch_time = t[glitch_idx]
                thresh_crossing = min(glitch_time, thresh_crossing)


        if glitch_count > 1:
            event.add_tag('BRS_GLITCH')
        else:
            logging.info('No %s BRS glitching detected' % (endstation))

        fig, ax = plt.subplots(1, figsize=(22,16))
        for buf in brs_channels:
            srate = buf.sample_rate
            t = np.arange(segment[0], segment[1], 1/srate)
            ax.plot(
                t-event.gps,
                buf.data,
                label=buf.channel,
                alpha=0.8,
                lw=2,
            )
        ax.axhline(
            config.BRS_THRESH,
            linestyle='--',
            color='black',
            label='BRS glitch threshold',
            lw=5,
        )
        if thresh_crossing != segment[1]:
            plotutils.set_thresh_crossing(ax, thresh_crossing, event.gps, segment)

        ax.grid()
        ax.set_xlabel('Time [s] since lock loss at {}'.format(event.gps), labelpad=10)
        ax.set_ylabel('RMS Velocity [nrad/s]')
        ax.set_ylim(0, max_brs+1)
        ax.set_xlim(t[0]-event.gps, t[-1]-event.gps)
        ax.legend(loc='best')
        ax.set_title('%s BRS BLRMS' % (endstation), y=1.04)
        fig.tight_layout(pad=0.05)

        outfile_plot = 'brs_%s.png' % (endstation)
        outpath_plot = event.gen_path(outfile_plot)
        fig.savefig(outpath_plot, bbox_inches='tight')
