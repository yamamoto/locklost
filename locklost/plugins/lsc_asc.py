import numpy as np
import matplotlib.pyplot as plt

from gwpy.segments import Segment

from .. import config
from .. import data
from .. import plotutils

##############################################

def plot_lsc_asc(event):
    """Plot LSC/ASC channels

    """
    gps = event.gps
    plotutils.set_rcparams()

    for chan_group, channels in config.LSC_ASC_CHANNELS.items():
        for window_type, window in config.PLOT_WINDOWS.items():
            segment = Segment(window).shift(gps)
            bufs = data.fetch(channels, segment)
            for idx, buf in enumerate(bufs):
                color = 'royalblue'
                name = buf.channel
                srate = buf.sample_rate
                t = np.arange(window[0], window[1], 1/srate)
                fig, ax = plt.subplots(1, figsize=(18,13))
                if event.refined:
                    status = 'refined'
                else:
                    status = 'unrefined'
                ax.set_xlabel('Time [s] since {} lock loss at {}'.format(status, gps),
                              labelpad=10)
                ax.set_ylabel('Counts')
                ax.set_xlim(window)
                ymin, ymax = calc_ylims(t, buf.data)
                ax.set_ylim(ymin, ymax)
                ax.grid()
                ax.plot(t, buf.data, color = color, label = name, linewidth=2.5)
                ax.legend()

                fig.tight_layout()

                #saves plot to lockloss directory
                outfile_plot = '{}_{}.png'.format(name, window_type)
                outpath_plot = event.gen_path(outfile_plot)
                fig.savefig(outpath_plot, bbox_inches='tight')
                fig.clf()


def calc_ylims(t, y):
    """Calculate ylims for the section of SUS channels occuring before lock loss time.

    """
    scaling_times = np.where(t < 0)[0]
    scaling_vals = y[scaling_times]
    ymin, ymax = min(scaling_vals), max(scaling_vals)

    if ymax > 0:
        ymax = 1.1*ymax
    else:
        ymax = 0.9*ymax
    if ymin < 0:
        ymin = 1.1*ymin
    else:
        ymin = 0.9*ymin

    if ymin == ymax:
        ymin, ymax = -0.2, 0.2

    return ymin, ymax


def is_null_channel(data):
    """True if all data is zero

    """
    null = None
    if all(data == 0):
        null = True
    else:
        null = False
    return null
