import collections
from gwpy.segments import Segment

import matplotlib.pyplot as plt
from matplotlib import rcParamsDefault
plt.rcParams.update(rcParamsDefault)


FOLLOWUPS = collections.OrderedDict()
def add_follow(mod):
    FOLLOWUPS.update([(mod.__name__, mod)])

from .discover import discover_data
add_follow(discover_data)

from .refine import refine_time
add_follow(refine_time)

from .observe import check_observe
add_follow(check_observe)

from .history import find_previous_state
add_follow(find_previous_state)

from .saturations import find_saturations
add_follow(find_saturations)

from .lpy import find_lpy
add_follow(find_lpy)

from .lsc_asc import plot_lsc_asc
add_follow(plot_lsc_asc)

from .glitch import analyze_glitches
add_follow(analyze_glitches)

from .overflows import find_overflows
add_follow(find_overflows)

from .brs import check_brs
add_follow(check_brs)

from .board_sat import check_boards
add_follow(check_boards)

from .wind import check_wind
add_follow(check_wind)

from .seismic import check_seismic
add_follow(check_seismic)

from .ads_excursion import check_ads
add_follow(check_ads)
