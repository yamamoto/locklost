import os
import glob
import numpy as np
from contextlib import closing
import logging

from lal import gpstime
import nds2
import gwpy.timeseries

from . import config

##################################################

def nds_connection():
    try:
        HOSTPORT = os.getenv('NDSSERVER').split(',')[0].split(':')
    except AttributeError:
        raise SystemExit("NDSSERVER environment variable not specified.")
    HOST = HOSTPORT[0]
    try:
        PORT = int(HOSTPORT[1])
    except IndexError:
        PORT = 31200
    logging.debug("NDS connect: {}:{}".format(HOST, PORT))
    conn = nds2.connection(HOST, PORT)
    conn.set_parameter('GAP_HANDLER', 'STATIC_HANDLER_NAN')
    # conn.set_parameter('ITERATE_USE_GAP_HANDLERS', 'false')
    return conn


def nds_fetch(channels, start, stop):
    with closing(nds_connection()) as conn:
        bufs = conn.fetch(start, stop, channels)
        return [ChannelBuf.from_nds(buf) for buf in bufs]


def nds_iterate(channels, start_end=None):
    if start_end:
        args = list(start_end)
    else:
        args = []
    # FIXME: nds2 0.16 has the following:
    # args += [nds2.connection.FAST_STRIDE]
    # args += [channels]
    with closing(nds_connection()) as conn:
        if conn.get_protocol() == 1:
            stride = -1
        else:
            stride = 1
        args += [stride]
        args += [channels]
        for bufs in conn.iterate(*args):
            yield [ChannelBuf.from_nds(buf) for buf in bufs]

##################################################

class ChannelBuf(object):
    def __init__(self, channel, data, gps_start, gps_nanoseconds, sample_rate):
        self.channel = channel
        self.data = data
        self.gps_start = gps_start
        self.gps_nanoseconds = gps_nanoseconds
        self.sample_rate = sample_rate

    def __str__(self):
        return '<{channel} (GPS time {gps}, {sec} sec, {nsamples} samples)>'.format(
            channel=self.channel,
            gps=self.gps_start,
            sec=self.duration,
            nsamples=len(self),
            )

    def __len__(self):
        return len(self.data)

    @property
    def duration(self):
        return int(len(self) * (1/self.sample_rate))

    @property
    def gps_stop(self):
        return self.gps_start + self.duration

    @property
    def tarray(self):
        return np.arange(len(self)) * (1/self.sample_rate) + self.gps_start

    def yt(self, i=None):
        """Return (data, time) tuple

        If index `i` is specified, return (data, time) at specific
        index.  Otherwise return full data arrays.

        """
        if i is None:
            y = self.data
            t = self.tarray
        else:
            y = self.data[i]
            t = self.gps_start + self.gps_nanoseconds + i/self.sample_rate
        return y, t

    @classmethod
    def from_nds(cls, buf):
        return cls(buf.channel,
                   buf.data,
                   buf.gps_seconds,
                   buf.gps_nanoseconds,
                   buf.channel.sample_rate,
                   )

    @classmethod
    def from_frdata(cls, data):
        return cls(data.metadata.name,
                   data,
                   data.metadata.segments[0][0],
                   0,
                   1/data.metadata.dt,
                   )

    @classmethod
    def from_gwTS(cls, data):
        return cls(data.channel.name,
                   data.value,
                   data.t0.value,
                   0,
                   data.sample_rate.value,
                   )

##################################################

def frame_fetch(channels, start, stop):
    conn = glue.datafind.GWDataFindHTTPConnection()
    cache = conn.find_frame_urls(config.IFO[0], IFO+'_R', start, stop, urltype='file')
    fc = frutils.FrameCache(cache, verbose=True)
    return [ChannelBuf.from_frdata(fc.fetch(channel, start, stop)) for channel in channels]


def frame_fetch_gwpy(channels, start, stop):
    gps_now = int(gpstime.tconvert('now'))

    ### grab from shared memory if data is still available, otherwise use datafind
    if gps_now - start < config.MAX_QUERY_LATENCY:
        frames = glob.glob("/dev/shm/lldetchar/{}/*".format(config.IFO))
        data = gwpy.timeseries.TimeSeriesDict.read(frames, channels, start=start, end=stop)
    else:
        data = gwpy.timeseries.TimeSeriesDict.find(channels, start, stop, frametype=config.IFO+'_R')
    return [ChannelBuf.from_gwTS(data[channel]) for channel in channels]


def fetch(channels, segment):
    start = segment[0]
    stop = segment[1]
    method = config.DATA_ACCESS.lower()
    if method == 'nds':
        func = nds_fetch
    elif method == 'fr':
        func = frame_fetch
    elif method == 'gwpy':
        func = frame_fetch_gwpy
    else:
        raise ValueError("unknown data access method: {}".format(DATA_ACCESS))
    logging.debug("{}({}, {}, {})".format(func.__name__, channels, start, stop))
    return func(channels, start, stop)

##################################################

def gen_transitions(buf, previous=None):
    """Generator of transitions in ChannelBuf

    For every value change in `buf`, yield (gps, pval, val) tuple
    where pval is the value immediately before the transition value.

    `previous` should be the previous buffer, used to find transitions
    at the buffer boundary.

    """
    if previous:
        last = previous.data[-1]
    else:
        last = buf.data[0]
    # concatenate data
    data = np.append([last], buf.data)
    yd = np.diff(data)
    # transition indices in the above diff array correspond to the
    # indices of the transition in the original data
    ti = np.where(yd != 0)[0]
    for idx, i in enumerate(ti):
        val, time = buf.yt(i)
        # the previous value is the value of the concatenated data at
        # the transition index
        pval = data[i]
        yield (time, pval, val)
